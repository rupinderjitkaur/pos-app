var AngularApp = angular.module('AngularApp', ['ui.router']);

AngularApp.config(['$stateProvider','$urlRouterProvider',function($stateProvider, $urlRouterProvider) {

    $stateProvider
        // HOME STATES AND NESTED VIEWS ========================================

        .state('home', {
            url: '/home',
            templateUrl: 'app/templates/home.html',
            controller: 'PosController'
        })
  $urlRouterProvider.otherwise('/home');
}]);
